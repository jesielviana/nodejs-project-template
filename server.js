const Config = require('./src/config/config');
const setupApp = require('./src/app');

console.info('port: ', Config.PORT);

setupApp()
  .then((app) => app.listen(Config.PORT, () => console.info(`app running on port ${Config.PORT}`)))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
